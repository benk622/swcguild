

function hideEndScreen(){
	document.getElementById("endScreen").style.display = "none";
}


function playGame(){
	var bet = document.getElementById("bet");
	var betvalue = parseInt(bet.value);
	var rollCount = 0;
	var rollCountHigh = 0;
	var money = betvalue;
	var highestMoney = betvalue;
	var loss = false;
	
	hideEndScreen();

	while(loss == false){
		if (money > 0){
			rollCount++
			rollDice();
			if(money > betvalue && money > highestMoney){
				highestMoney = money;
				rollCountHigh = rollCount;
			}
			
		}
		else{
			loss = true;
		}
	}
	
	showResults();
	resetGame();
	
	function rollDice(){
	var die1 = 1 + Math.floor(Math.random() * 6);
	var die2 = 1 + Math.floor(Math.random() * 6);
	
		if (die1 + die2 == 7){
			money = money + 4;
		} else{
		money = money - 1;
		}
	
	}
	
	function showResults(){
	document.getElementById("endScreen").style.display = "block";
	document.getElementById("startBet").innerHTML ="$" + betvalue + ".00";
	document.getElementById("totalRolls").innerHTML = rollCount;
	document.getElementById("highestAmount").innerHTML = "$" + highestMoney + ".00";
	document.getElementById("rollCountHighest").innerHTML = rollCountHigh;
	
	}
	
	function resetGame(){
		play.innerHTML = "Play Again";
		betvalue = 0;
	}
	
	

}








