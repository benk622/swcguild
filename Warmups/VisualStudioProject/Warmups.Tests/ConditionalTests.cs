﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class ConditionalTests
    {

        Conditionals obj = new Conditionals();

        [TestCase(true, true, true)]
        [TestCase(false, false, true)]
        [TestCase(true, false, false)]
        public void AreWeInTroubleTest(bool aSmile, bool bSmile, bool expected)
        {
            // arrange
            Conditionals obj = new Conditionals();

            // act
            bool actual = obj.AreWeInTrouble(aSmile, bSmile);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(23, 4)]
        [TestCase(10, 11)]
        [TestCase(21, 0)]
        public void Diff21Test(int n, int expected)
        {
            Conditionals obj = new Conditionals();

            int actual = obj.Diff21(n);

            Assert.AreEqual(expected, actual);
        }

        [TestCase (true, 6, true)]
        [TestCase(true, 7, false)]
        [TestCase(false, 6, false)]
        public void ParrotTroubleTest(bool isTalking, int hour, bool expected)
        {

            bool actual = obj.ParrotTrouble(isTalking, hour);

            Assert.AreEqual(expected, actual);


        }
        
        [TestCase (9,10,true)]
        [TestCase(9, 9, false)]
        [TestCase(1, 9, true)]
        public void Makes10Test(int a, int b, bool expected)
        {
            bool actual = obj.Makes10(a, b);

            Assert.AreEqual(expected, actual);

        }

        [TestCase(103, true)]
        [TestCase(90, true)]
        [TestCase(89, false)]
        public void NearHundredTest(int n, bool expeceted)
        {
            bool actual = obj.NearHundred(n);

            Assert.AreEqual(expeceted, actual);
        }

        [TestCase(1, -1, false, true)]
        [TestCase(-1, 1, false, true )]
        [TestCase(-4, -5, true, true)]
        public void PosNegTest(int a, int b, bool negative, bool expected)
        {
            bool actual = obj.PosNeg(a, b, negative);

            Assert.AreEqual(expected, actual);

        }
    }
}
