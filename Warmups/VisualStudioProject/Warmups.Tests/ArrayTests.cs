﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class ArrayTests
    {
        // arrange
        Arrays obj = new Arrays();

        [TestCase(new int[] { 1, 2, 6 }, true)]
        [TestCase(new int[] { 6, 1, 2, 3 }, true)]
        [TestCase(new int[] { 13, 6, 1, 2, 3 }, false)]
        public void FirstLast6Test(int[] numbers, bool expected)
        {


            // act
            bool actual = obj.FirstLast6(numbers);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] { 1, 2, 3 }, 6)]
        [TestCase(new int[] { 5, 11, 2 }, 18)]
        [TestCase(new int[] { 7, 0, 0 }, 7)]
        public void SumTest(int[] numbers, int expected)
        {

            int actual = obj.Sum(numbers);

            Assert.AreEqual(expected, actual);

        }


        [TestCase(new int[] { 1, 2, 3 }, new int[] { 2, 3, 1 })]
        [TestCase(new int[] { 5, 11, 9 }, new int[] { 11, 9, 5 })]
        [TestCase(new int[] { 7, 0, 0 }, new int[] { 0, 0, 7 })]
        public void RotateLeftTest(int[] numbers, int[] expected)
        {
            int[] actual = obj.RotateLeft(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] { 3, 2, 1 })]
        public void ReverseTest(int[] numbers, int[] expected)
        {
            int[] actual = obj.Reverse(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] { 3, 3, 3 })]
        [TestCase(new int[] { 11, 5, 9 }, new int[] { 11, 11, 11, })]
        [TestCase(new int[] { 2, 11, 3}, new int[] { 3, 3, 3 })]
        public void HigherWins(int[] numbers, int[] expected)
        {
            int[] actual = obj.HigherWins(numbers);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(new int[] { 1, 2, 3 }, new int[] { 4, 5, 6}, new int[] { 2, 5 })]
        [TestCase(new int[] { 7, 7, 7 }, new int[] { 3, 8, 0 }, new int[] { 7, 8 })]
        [TestCase(new int[] { 5, 2, 9 }, new int[] { 1, 4, 5 }, new int[] { 2, 4 })]
        public void GetMiddleTest(int[] a, int[] b, int[] expected)
        {
            int[] actual = obj.GetMiddle(a, b);

            Assert.AreEqual(expected, actual);

        }

        [TestCase(new int[] { 2, 5 }, true)]
        [TestCase(new int[] { 4, 3 }, true )]
        [TestCase(new int[] { 7, 5, 3, 1, 11, 15}, false)]
        public void HasEvenTest(int[] numbers, bool expected)
        {

            bool actual = obj.HasEven(numbers);

            Assert.AreEqual(expected, actual);

        }
        [TestCase(new int[] { 4, 5, 6 }, new int[] { 0, 0, 0, 0, 0, 6 })]
        [TestCase(new int[] { 1, 2 }, new int[] { 0, 0, 0, 2 })]
        [TestCase(new int[] { 3 }, new int[] { 0, 3 })]
        public void KeepLastTest(int[] numbers, int[] expected)
        {
            int[] actual = obj.KeepLast(numbers);

            Assert.AreEqual(expected, actual);

        }
    }
}
