﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class LogicTests
    {
        Logic obj = new Logic();

        [TestCase(30, false, false)]
        [TestCase(50, false, true)]
        [TestCase(70, true, true)]
        public void GreatPartyTest(int cigars, bool isWeekend, bool expected)
        {
            // arrange
            Logic obj = new Logic();

            // act
            bool actual = obj.GreatParty(cigars, isWeekend);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase(5, 10, 2)]
        [TestCase(5, 2, 0)]
        [TestCase(5,5,1)]
        public void CanHazTableTest(int yourstyle, int datestyle, int expected)
        {
            int actual = obj.CanHazTable(yourstyle, datestyle);

            Assert.AreEqual(expected, actual);

        }

        [TestCase(70, false, true)]
        [TestCase(95, false, false )]
        [TestCase(95, true, true)]
        public void PlayOutsideTest(int temp, bool isSummer, bool expected)
        {
            bool actual = obj.PlayOutside(temp, isSummer);

            Assert.AreEqual(expected, actual);


        }

        [TestCase(60, false, 0)]
        [TestCase(65, false, 1)]
        [TestCase(65, true, 0)]
        public void CaughtSpeedingTest(int speed, bool isBirthday, int expected)
        {
            int actual = obj.CaughtSpeeding(speed, isBirthday);

            Assert.AreEqual(expected, actual);
        }

        [TestCase(3, 4, 7)]
        [TestCase(9, 4, 20)]
        [TestCase(10, 11, 21)]
        public void SkipSum(int a, int b, int expected)
        {
            int actual = obj.SkipSum( a, b);

            Assert.AreEqual(expected, actual);

        }


    }
}
