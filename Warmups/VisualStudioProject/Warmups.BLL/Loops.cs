﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Loops
    {
        /* Given a string and a non-negative int n, return a 
           larger string that is n copies of the original string. 
        */
        public string StringTimes(string str, int n)
        {
            string result = "";

            for (int i = 1; i <= n; i++)
            {
                result += str;
            }

            return result;
        }


        /* Given a string and a non-negative int n, we'll say that the 
         * front of the string is the first 3 chars, or whatever is there 
         * if the string is less than length 3. Return n copies of the front; 
         */


        public string FrontTimes(string str, int n)
        {
            string front = str.Substring(0, 3);
            
            string why = null;

            for (int i = 0; i < n; i++)
            {

                why += front;
            }

            return why;

     

        }

        /* Count the number of "xx" in the given string. We'll say that
         *  overlapping is allowed, so "xxx" contains 2 "xx". 
         */

        public int CountXX(string str)
        {
            int count = 0;

            for (int i = 0; i < str.Length-1; i++)
            {
                if (str.Substring(i, 2) == "xx")
                {
                    count++;

                }

            }

            return count;
           
        }

        /* Given a string, return true if the first instance of "x" in the 
         * string is immediately followed by another "x". 
         */
         public bool DoubleX(string str)
        {



            for (int i = 0; i < str.Length; i++)
            {
                if(str.Substring(i, 1) == "x" && str.Substring(i+1, 1) == "x" )
                {
                    return true;
                    
                }
                else if (str.Substring(i, 1) == "x" && str.Substring(i+1, 1) != "x")
                {
                    return false;
                }
                else
                {

                }
              
            }

            return false;

        }

        /* Given a string, return a new string made of every other char 
         * starting with the first, so "Hello" yields "Hlo".*/
         
        public string EveryOther(string str)
        {
            string final = "";

            for (int i = 0; i < str.Length; i++)
            {
                if(i % 2 == 0)
                {
                    final += str.Substring(i, 1);
                }
                else
                {

                }
            }

            return final;

        }

        /*Given a non-empty string like "Code" return a string like 
         * "CCoCodCode".  (first char, first two, first 3, etc) */

        public string StringSplosion(string str)
        {
            string final = "";

            for (int i = 1; i <= str.Length; i++)
            {
                final += str.Substring(0, i);

                

            }

            return final;

        }

        /* Given a string, return the count of the number of times that 
         * a substring length 2 appears in the string and also as the last 
         * 2 chars of the string, so "hixxxhi" yields 1 (we won't count the end substring). */

        public int CountLast2(string str)
        {


            int count = 0;

            for (int i = 0; i < str.Length-2; i++)
            {
                if(str.Substring(i, 2) == str.Substring(str.Length-2, 2))
                {
                    count++;
                }
                else
                {

                }

            }

            return count;

        }

        /* Given an array of ints, return the number of 9's in the array. */

        public int Count9(int[] numbers)
        {
            int nines = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if(numbers[i] == 9)
                {
                    nines++;
                }

            }

            return nines;

        }


    }
}
