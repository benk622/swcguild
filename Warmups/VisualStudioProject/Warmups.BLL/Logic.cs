﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Logic
    {
        /* When squirrels get together for a party, they like to have cigars. 
           A squirrel party is successful when the number of cigars is between 
           40 and 60, inclusive. Unless it is the weekend, in which case there is 
           no upper bound on the number of cigars. Return true if the party with 
           the given values is successful, or false otherwise. 
        */
        public bool GreatParty(int cigars, bool isWeekend)
        {
            if (isWeekend)
                return cigars > 40;
            else
                return (cigars >= 40 && cigars <= 60);
        }

        /*  You and your date are trying to get a table at a restaurant. 
            The parameter "you" is the stylishness of your clothes, 
            in the range 0..10, and "date" is the stylishness of your date's clothes. 
            The result getting the table is encoded as an int value with 0=no, 1=maybe, 2=yes. 
            If either of you is very stylish, 8 or more, then the result is 2 (yes). 
            With the exception that if either of you has style of 2 or less, then the result is 0 (no). 
            Otherwise the result is 1 (maybe). 
        */

        public int CanHazTable(int yourStyle, int dateStyle)
        {
            if ((yourStyle >= 8 || dateStyle >= 8) && (yourStyle > 2 && dateStyle > 2))
            {
                return 2;
            }
            else if (yourStyle <= 2 || dateStyle <= 2)
            {
                return 0;
            }

            return 1;
        }

        /*The children in Cleveland spend most of the day playing outside. 
        In particular, they play if the temperature is between 60 and 90 (inclusive). 
        Unless it is summer, then the upper limit is 100 instead of 90. 
        Given an int temperature and a bool isSummer, 
        return true if the children play and false otherwise.
        */

        public bool PlayOutside(int temp, bool isSummer)
        {
            if (temp < 60)
            {
                return false;

            }
            else if (temp <= 90)
            {
                return true;
            }
            else if (isSummer == true && temp <= 100)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /*You are driving a little too fast, and a police officer stops you. Write code to 
         * compute the result, encoded as an int value: 0=no ticket, 1=small ticket, 2=big ticket. 
         * If speed is 60 or less, the result is 0. If speed is between 61 and 80 inclusive, 
         * the result is 1. If speed is 81 or more, the result is 2. Unless it is your birthday -- 
         * on that day, your speed can be 5 higher in all cases. */

        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            if(speed <= 60 && isBirthday== false|| (isBirthday == true && speed <= 65))
            {
                return 0;
            }
            else if(speed >= 61 && speed <= 80 && isBirthday == false || (isBirthday == true && speed >= 66 && speed <= 85))
            {
                return 1;
            }
            else if(isBirthday == false && speed >= 81)
            {
                return 2;
            }
            else
            {
                return 2;
            }
            

        }

        /* Given 2 ints, a and b, return their sum. However, sums in the range 10..19 inclusive are forbidden, 
         * so in that case just return 20.*/

        public int SkipSum(int a, int b)
        {
            int newSum = a + b;

            if(newSum >= 10 && newSum <= 19)
            {
                newSum = 20;
                
            }

            return newSum;

        }



    }
}
