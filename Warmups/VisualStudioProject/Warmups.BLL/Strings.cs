﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Strings
    {
        // Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!". 
        public string SayHi(string name)
        {
            return string.Format("Hello {0}!", name);
        }


        /* Given an "out" string length 4, such as "<<>>", and a word, return a 
         * new string where the word is in the middle of the out string, e.g. "<<word>>". */

        public string InsertWord(string container, string word)
        {
            string output = container.Substring(0, 2) + word + container.Substring(2, 2);

            return output;
        }

        /* Given a string, return a new string made of 3 copies of the last 2 chars of the original string. 
         * The string length will be at least 2. */

        public string MultipleEndings(string str)
        {
            string lastTwo = str.Substring(str.Length - 2, 2);

            string output = "";

            for (int i = 0; i < 3; i++)
            {
                output += lastTwo;
                    

            }

            return output;
        }

        /* Given a string, return a version without the first and last char, so "Hello" yields "ell".
         *  The string length will be at least 2. */

        public string TrimOne(string str)
        {
            string output = str.Substring(1, str.Length - 2);

            return output;
               
        }

        /* Given 2 strings, a and b, return a string of the form short+long+short, with the 
         * shorter string on the outside and the longer string on the inside. 
         * The strings will not be the same length, but they may be empty (length 0). */

        public string LongInMiddle(string a, string b)
        {
            string output = "";
            if (a.Length < b.Length)
            {
                output = a + b + a;
            }
            else
            {
                output = b + a + b;
            }

            return output;
        }

    }
}
