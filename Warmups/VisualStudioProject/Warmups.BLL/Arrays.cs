﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Arrays
    {
        /* Given an array of ints, return true if 6 appears 
           as either the first or last element in the array. 
           The array will be length 1 or more. 
        */
        public bool FirstLast6(int[] numbers)
        {
            // 0 is always the first index and 
            // Length - 1 of an array is always the last index
            return (numbers[0] == 6 || numbers[numbers.Length - 1] == 6);
        }

        /* Given an array of ints, return the sum of all the elements. */

        public int Sum(int[] numbers)
        {
            int answer = 0;

            for (int i = 0; i < numbers.Length; i++)
            {

                answer += numbers[i];

            }

            return answer;


        }

        /* Given an array of ints, return an array with the elements "rotated left" so 
         * {1, 2, 3} yields {2, 3, 1}. 
         */

        public int[] RotateLeft(int[] numbers)
        {
            int[] rotateArray = new int[numbers.Length];

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] == numbers[0])
                {
                    rotateArray[numbers.Length - 1] = numbers[i];
                }
                else
                {
                    rotateArray[i - 1] = numbers[i];
                }



            }

            return rotateArray;
        }

        /* Given an array of ints length 3, return a new array with the elements in reverse order, 
         * so for example {1, 2, 3} becomes {3, 2, 1}. */

        public int[] Reverse(int[] numbers)
        {
            int[] reverseArray = new int[numbers.Length];

            for (int i = 0; i < reverseArray.Length; i++)
            {
                if (i == 0)
                {
                    reverseArray[0] = numbers[reverseArray.Length - 1];

                }
                else if (i == reverseArray.Length - 1)
                {
                    reverseArray[reverseArray.Length - 1] = numbers[0];
                }
                else
                {
                    reverseArray[i] = numbers[i];
                }


            }
            return reverseArray;
        }

        /* Given an array of ints, figure out which is larger between the first and last elements
         *  in the array, and set all the other elements to be that value. Return the changed array. */

        public int[] HigherWins(int[] numbers)
        {
            int[] finalArray = new int[3];

            if (numbers[0] > numbers[numbers.Length - 1])
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    finalArray[i] += numbers[0];
                    
                }
                return finalArray;
            }
            else if (numbers[numbers.Length - 1] > numbers[0])
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    finalArray[i] += numbers[numbers.Length - 1];
                    
                }

                return finalArray;
            }
            else
            {
                return numbers;
            }





        }

        /* Given 2 int arrays, a and b, each length 3, return a new array length 2 containing 
         * their middle elements. */

        public int[] GetMiddle(int[] a, int[] b)
        {
            int[] newArray = new int[2] { a[1], b[1] };
            return newArray;

        }

        /* Given an int array , return true if it contains an even number (HINT: Use Mod (%)). */

        public bool HasEven(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if(numbers[i] % 2 == 0)
                {
                    return true;
                }
               

            }

            return false;
        }

        /* Given an int array, return a new array with double the length where its last element is the same as the 
         * original array, and all the other elements are 0. The original array will be length 1 or more.
         *  Note: by default, a new int array contains all 0's. */

        public int[] KeepLast(int[] numbers)
        {
            int[] output = new int[numbers.Length * 2];

            for (int i = 0; i < output.Length; i++)
            {
                if(i != output.Length-1)
                {
                    output[i] = 0;
                }
                else
                {
                    output[i] = numbers[numbers.Length - 1];
                }

            }
            return output;
        }

    }
}
 
 
 