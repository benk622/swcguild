﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Conditionals
    {
        /* 1. We have two children, a and b, and the parameters aSmile and 
           bSmile indicate if each is smiling. We are in trouble if they 
           are both smiling or if neither of them is smiling. Return true 
           if we are in trouble. 
        */
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            if (aSmile && bSmile)
                return true;

            if (!aSmile && !bSmile)
                return true;

            return false;
        }

        /* 4. Given an int n, return the absolute value of the difference between n and 21,
         *  except return double the absolute value of the difference if n is over 21. 
         */

        public int Diff21(int n)
        {
            if (n <= 21)
            {
                return 21 - n;
            }
            else
            {
                return (n - 21) * 2;
            }

        }

        /* We have a loud talking parrot. The "hour" parameter is the current hour time in the range 0..23. 
         * We are in trouble if the parrot is talking and 
         * the hour is before 7 or after 20. Return true if we are in trouble. 
         */

        public bool ParrotTrouble(bool isTalking, int hour)
        {
            if(isTalking == false)
            {
                return false;

            }
            else if (hour >= 7 && hour <= 20)
            {
                return false;

            }
            else
            {
                return true;
            }


        }

        /*Given two ints, a and b, return true if one if them is 10 or if their sum is 10. */

        public bool Makes10(int a, int b)
        {
            if(a == 10 || b == 10 || (a+b ==10))
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        /*Given an int n, return true if it is within 10 of 100 or 200.
    Hint: Check out the C# Math class for absolute value */

        public bool NearHundred(int n)
        {
            if ((n >= 90 && n <= 110 )|| (n>= 190 && n <= 210))
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        /* Given two int values, return true if one is negative and one is positive.
         *  Except if the parameter "negative" is true, then return true only if both are negative. */

        public bool PosNeg(int a, int b, bool negative)
        {
            if((a < 0 && b > 0) && negative == false)
            {
                return true;
            }
            else if((a > 0 && b < 0) && negative == false)
            {
                return true;
            }
            else if((a < 0 && b < 0 ) && negative == true)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


    }
}
